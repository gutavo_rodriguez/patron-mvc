﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodigoExposicion.Models
{
    public class Producto
    {
        public int CantidadProducto { get; set; }
        public string TipoDeProducto { get; set; }
        public string MarcaDelPorducto { get; set; }
        public string CiudadDeProcedencia { get; set; }
    }
}